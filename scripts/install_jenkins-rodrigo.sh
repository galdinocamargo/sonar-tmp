#!/bin/bash

# Version: 2
# this script is only tested on ubuntu xenial

# Changed by galdinocamargo@gmail.com

# run jenkins (on windows)
mkdir -p c:/var/jenkins_home
chown -R 1000:1000 c:/var/jenkins_home/
docker run -p 8080:8080 -p 50000:50000 -v c:/var/jenkins_home:/var/jenkins_home -d --name jenkins jenkins/jenkins:lts


# show endpoint
echo 'Jenkins installed'
echo 'You should now be able to access jenkins at: http://'$(curl -s ifconfig.co)':8080'
